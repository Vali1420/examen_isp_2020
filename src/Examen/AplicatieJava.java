package Examen;
//Al doilea exercitiu

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AplicatieJava extends JFrame {

    JTextField produs;
    JTextArea a;
    JTextArea b;
    JButton button;
    JLabel x;


    AplicatieJava(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Calculator produs");
        init();
        setVisible(true);
        setSize(300,200);
    }

    public void init() {
        setLayout(null);

        int height=20;
        int width=60;

        a=new JTextArea();
        a.setBounds(25,50,width,height);

        b=new JTextArea();
        b.setBounds(100,50,width,height);

        x=new JLabel("x");
        x.setBounds(90,50,10,20);

        produs=new JTextField();
        produs.setBounds(175,50,width,height);
        produs.setEditable(false);

        button=new JButton("Produs");
        button.setBounds(80,100,width+40,height);
        button.addActionListener(new ComportamentButon());


        add(a);add(b);add(produs);add(button);add(x);


    }
    class ComportamentButon implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e) {

            produs.setText(String.valueOf(Double.parseDouble(a.getText())* Double.parseDouble(b.getText())));

        }
    }

    public static void main(String[] args) {
        new AplicatieJava();
    }

}
